@Grab("org.gebish:geb-core:0.9.2")
@Grab("org.seleniumhq.selenium:selenium-chrome-driver:2.37.1")
@Grab("org.seleniumhq.selenium:selenium-support:2.37.1")

import geb.Browser

System.setProperty("webdriver.chrome.driver", "/Users/david/Desktop/selenium/chromedriver")
 
Browser.drive {
    go "http://www.google.com"
     
    assert $("h1").text() == "Please Login"
     
    $("form.login").with {
        username = "admin"
        password = "password"
        login().click()
    }
     
    assert $("h1").text() == "Admin Section"
}